column_number_map = new Map([
['Type_of_loan_applicant', 1],
['residence_type', 2],
['gst_available_1', 3],
['ITR_Availability', 4],
['rented_proof_available', 5],
['permanent_residence_type', 6],
['Profession_1', 7],
['credit_score', 8],
['sf_wd_wo_settled_amount', 9],
['Auto_Default_AD', 10],
['ITR_duration', 11],
['months_residing_current', 12],
['ownership_additional_property', 13],
['Nature_of_Profession', 14],
['max_dpd_last_36_months_cc_other', 15],
['gender', 16],
['Husband_Or_Father_Default', 17],
['Resicum_office', 18],
['rank_police', 19],
['Pension_proof_available', 20],
['Salary_slips_availability_last3months', 21],
['present_employer_salary_source', 22],
['permanent_address_type', 23],
['additional_income', 24],
['land_size', 25],
['total_overdue_cc_other', 26],
['Other_Owned_property', 27],
['Salary_slips_available', 28],
['gst_available', 29],
['number_of_sf_wd_wo_settled_accounts', 30],
['fixed_income_exclusive', 31],
['ITR_value_1', 32]
])

OUTPUTS_COL_NUM = 34
// SPREADSHEET_ID = '1LAg6j6H0oRTSnFL2MPG_vX2wYxiqZ37V5OoXK7iKj-I'
SPREADSHEET_ID = '1eNxg21__PGxQhHBLWr8ZcMlspepk_mNsA8Ynj_A3sWc'

function put_dmn_to_sheet(conditions, combination_bucket, bucket_id, till) {
  sheet1 = getSheetById(SPREADSHEET_ID, 'dmn')
  range1 = sheet1.getDataRange()
  range1.getCell(till, OUTPUTS_COL_NUM+1).setValue(bucket_id)
  range1.getCell(till, OUTPUTS_COL_NUM+2).setValue(combination_bucket)
  conditions.forEach((value, key) =>{
      range1.getCell(till, column_number_map.get(key)+1).setValue(value)
  })
}

function generateDMN() {
  sheet = getSheetById(SPREADSHEET_ID, 'vbc_1')
  range = sheet.getDataRange()
  till = 6 //6
  for (i = 2; i <= range.getNumRows();) { //i=2
    start = i;
    while(i <= range.getNumRows() && range.getCell(start,1).getValue() == range.getCell(i,1).getValue()) {
      i=i+1;
    }
    end = i-1;
    Logger.log(`${start} ${end}`)

    var conditions = new Map();
    combination_bucket = range.getCell(start,1).getValue()
    bucket_id = range.getCell(start, 2).getValue()
    for(j = start; j<=end;) {
      key = range.getCell(j, 4).getValue()
      condition = range.getCell(j, 5).getValue()

      values = []
      while(j<=end && key == range.getCell(j, 4).getValue()) {
        values.push(range.getCell(j, 8).getValue()) // 6
        j=j+1
      }

      if(values.length > 1) {
        for(var p=0; p<values.length; p++){
          values[p] = "\"" + values[p] + "\"";
        }
        if(condition == "!=") {
          value = `not(${values.join([separator = ', '])})`
        } else {
          value = values.join([separator = ', '])
        }
      } else {
        if(condition == "=") {
          value = values[0]
        } else if (condition == "!=") {
          value = `not("${values[0]}")`
        }
        else {
          value = `${condition} ${values[0]}`
        }
      }
      Logger.log(`${key} ${value}`)
      conditions.set(key, value)
    }
    Logger.log(`${start} ${end} ${[...conditions.entries()]}`)
    put_dmn_to_sheet(conditions, combination_bucket, bucket_id, till)
    till = till + 1
    // if (till > 40) {
    //   break
    // }
  }
}
