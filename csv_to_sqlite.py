import sqlite3
import pandas as pd
import csv
import json


def process(postfix):
    conn = sqlite3.connect('resources/ruleengine.db')
    cursor = conn.cursor()
    cursor.execute(f'CREATE TABLE IF NOT EXISTS answersheets_{postfix} (buyer_id int, question text, answer text)')

    answersheets = pd.read_csv(f'resources/raw/answers_{postfix}.csv')

    tuples = 0
    with open(f'resources/raw/cibil_report_{postfix}.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        head = True
        for row in csv_reader:

            if head or row[0] == '' or row[0] == 'null':
                head = False
                continue

            print(row)
            cibil_report = json.loads(row[0])
            buyer_id = int(row[1])

            for key in ['credit_score', 'sf_wd_wo_settled_amount', 'number_of_sf_wd_wo_settled_accounts',
                        'total_overdue_cc_other', 'max_dpd_last_36_months_cc_other']:
                if key in cibil_report:
                    answersheets = answersheets.append(
                        {'buyer_id': buyer_id, 'question': key, 'answer': cibil_report[key]},
                        ignore_index=True)
                    tuples += 1

    print(answersheets)
    print(f'Got rows from cibil_report.csv: {tuples}')
    answersheets.to_sql(f'answersheets_{postfix}', conn, if_exists='append', index=False)


def prepare_for_analysis(postfix):
    conn = sqlite3.connect('resources/ruleengine.db')
    cursor = conn.cursor()
    cursor.execute(f'CREATE TABLE IF NOT EXISTS analysis_{postfix} (buyer_id int, combination_id int)')

    data = []
    with open(f'resources/raw/cibil_report_{postfix}.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        head = True
        for row in csv_reader:

            if head:
                head = False
                continue

            data.append([int(row[1]), int(row[2])])

    df = pd.DataFrame(data, columns=['buyer_id', 'combination_id'])
    print(df)
    df.to_sql(f'analysis_{postfix}', conn, if_exists='append', index=False)


if __name__ == '__main__':
    postfix = '30_days'
    process(postfix)
    prepare_for_analysis(postfix)
