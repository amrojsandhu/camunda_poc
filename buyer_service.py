import requests
import sqlite3
import json
import time
from csv import writer

conn = sqlite3.connect('resources/ruleengine.db')
cursor = conn.cursor()


def writeResultToCSV(results, postfix):
    with open(f'resources/raw/output_{postfix}.csv', 'w') as f_object:
        writer_object = writer(f_object)

        writer_object.writerow(['buyer_id', 'feasibility', 'combination_id', 'bank_bucket', 'time_taken',
                                'correct_combination_id', 'is_a_match'])
        for k, v in results.items():
            list = [k, v['feasibility'], v['combination_id'], v['bank_bucket'], v['time_taken'],
                    v['correct_combination_id'], v['is_a_match']]
            writer_object.writerow(list)

        f_object.close()


def getBucketMapFromDB(postfix):
    cursor.execute(f'SELECT buyer_id, combination_id FROM analysis_{postfix}')
    buyers = cursor.fetchall()

    map = {}
    for row in buyers:
        map[row[0]] = row[1]

    return map


def getBuyerAnswers(postfix):
    results = {}

    cursor.execute(f'SELECT distinct buyer_id FROM answersheets_{postfix}')
    buyers = cursor.fetchall()

    combinationToBuyerMap = getBucketMapFromDB(postfix)

    # buyers = [[341106]]
    for buyer_list in buyers:
        buyerId = buyer_list[0]
        print(buyerId)

        cursor.execute(f'SELECT question, answer FROM answersheets_{postfix} where buyer_id = {buyerId}')
        rows = cursor.fetchall()
        # print(rows)

        variables = {}
        for row in rows:
            if row[1]:
                variables[row[0]] = {'value': row[1].strip(), 'type': 'string'}

        data = {'variables': variables}

        # print(f'data: {data}')
        headers = {'Content-Type': 'application/json', 'Accept': '*/*'}

        start = time.time()
        response = requests.post(url='http://localhost:8080/engine-rest/decision-definition/key/default/evaluate',
                                 json=data,
                                 headers=headers)
        taken = time.time() - start

        if 'type' in response.json():
            print(f'Exception for buyer: {buyerId}, {response.json()}')
            continue

        json = response.json()[0]
        print(f'response: {json}')

        combination_id = json['combination_id']['value'] if 'combination_id' in json else ''
        is_a_match = (combination_id == combinationToBuyerMap[buyerId])

        if not is_a_match:
            print(data)

        results[buyerId] = {'feasibility': json['feasibility']['value'] if 'feasibility' in json else 'NA',
                            'combination_id': combination_id,
                            'bank_bucket': json['bank_bucket']['value'] if 'bank_bucket' in json else '',
                            'time_taken': int(taken * 1000),
                            'is_a_match': is_a_match,
                            'correct_combination_id': combinationToBuyerMap[buyerId]}

    writeResultToCSV(results, postfix)


if __name__ == '__main__':
    getBuyerAnswers('30_days')
